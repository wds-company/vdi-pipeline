#!/bin/bash
#set -x
read lower_port upper_port < /proc/sys/net/ipv4/ip_local_port_range
    for (( port = lower_port ; port <= upper_port ; port++ )); do
	    result=$(docker container ls --format "table {{.Ports}}" -a |grep "0.0.0.0:$port" |wc -l)
	    if [ $result == 0 ]
	    then 
		    break
	    fi
    done
echo $port